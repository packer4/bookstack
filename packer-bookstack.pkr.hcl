packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.1"
      source  = "github.com/hashicorp/amazon"
    }
  }
}


source "amazon-ebs" "centos7" {
  ami_name                = "bookstack_{{ timestamp }}"
  ami_description         = "An AMI with PHP and other packages pre-installed for bookstack. Bookstack must be installed from source and depends on a databse, neither of which are included in this AMI."
  ami_virtualization_type = "hvm"

  instance_type               = "t3.micro"
  associate_public_ip_address = true
  source_ami                  = "ami-011939b19c6bd1492"
  //iam_instance_profile
  shutdown_behavior = "terminate"
  ssh_interface     = "public_ip"
  ssh_username      = "centos"

  launch_block_device_mappings {
    device_name           = "/dev/sda1"
    volume_type           = "gp3"
    volume_size           = 10
    delete_on_termination = true
  }
}

build {
  name = "bookstack"

  sources = [
    "source.amazon-ebs.centos7",
  ]

  provisioner "ansible" {
    playbook_file = "./provision.yml"
  }
}
